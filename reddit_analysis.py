import networkx as nx
import operator
import os
import sys
import time
import psutil
import reddit_page_rank as pr
# import matplotlib.pyplot as plt
from scipy.stats.stats import pearsonr
from _collections import defaultdict
from operator import sub



#https://storage.googleapis.com/shlomo_reddit_bucket/authors_result_small/author2author_count_small.csv

# AUTHOR_COUNTS_DATA_FOLDER = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/author_comment_counts/test/" ;
# AUTHOR_TO_AUTHOR_DATA_FOLDER = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/author_to_author_data/" ;
# PAGE_RANK_RESULTS_FOLDER = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/page_rank_results/"
# CORRELATION_RESULTS = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/correlation_results.csv"
# CORRELATION_RESULTS_TOP = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/correlation_results_top.csv"
# SUBREDDITS_FILE = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/subreddits.csv"

PAGE_RANK_BASE_FOLDER = "/home/vaadrahelemenu19/reddit/pagerank/"
AUTHOR_COUNTS_DATA_FOLDER = PAGE_RANK_BASE_FOLDER+"data/author_counts/"
AUTHOR_TO_AUTHOR_DATA_FOLDER = PAGE_RANK_BASE_FOLDER+"data/author_to_author_by_subreddit/" 
PAGE_RANK_RESULTS_FOLDER = PAGE_RANK_BASE_FOLDER +"data/page_rank_by_subreddit/"
CORRELATION_RESULTS = PAGE_RANK_BASE_FOLDER +"data/correlation_results.csv"
CORRELATION_RESULTS_TOP = PAGE_RANK_BASE_FOLDER +"data/correlation_results_top.csv"
SUBREDDITS_FILE = "/home/vaadrahelemenu19/reddit/pagerank/src/reddit/top100.csv"



def load_file():    #loading from file:
    with open(GRAPH_FULL_FILE_PATH) as f:
        lines = f.read().splitlines()
    return lines[1:] ; #Remove headers


    """
    Returns the value of given key (value is an int). 
    If key doesn't exist in dic, it is set as len(dic)+1
    """    
def get_index(dic, key):
    return dic.setdefault(key, len(dic)+1)       
        
class UserCount(object):
    def __init__(self, name=None, comments=0, score_sum=0, abs_score_sum=0):
        self.name = name
        self.comments = comments
        self.score_sum = score_sum
        self.abs_score_sum = abs_score_sum


def log(msg):
    print("%s\t%s."%(time.strftime("%c"), msg))



def load_comment_counts(data_folder):
    """
    Load the comments count from the given folder path.
    Should contain files with prefix 'comments_score_counts'
    with a header line and then values in the following order:
    a_author,a_subreddit_id,comment_count,a_score_sum,a_abs_score_sum
    where:
    a_author = the user name of the commenter (e.g. mattloch666)
    a_subreddit_id = id of the subreddit (e.g. t5_2fwo)
    comment_count = number of comments author had in specified subreddit
    a_score_sum = sum of scores (up votes - down votes) of all comments counted  
    a_abs_score_sum = sum of abs(score) of comments counted

    :param data_folder - absolute path of folder with data files.
    :return: object containing the data from the folders in the following representation: 
            dict(key = a_subreddit_id, value = dict(a_author, UserCount))
    """
    log("Loading comments")
    
    user_counts_by_subreddit = {}
    #Reading files line by line and creating counts dict:
    for filename in os.listdir(data_folder):
        if (filename.startswith("comment")):
            first_line = True
            with open(data_folder+filename) as f:
                log("loading file:"+filename)
                for line in f:
                    if(first_line):
                        first_line = False
                        continue 
                    #a_author,a_subreddit_id,comment_count,a_score_sum,a_abs_score_sum
                    line_split = line.strip().split(',');
                    
                    subreddit_counts = user_counts_by_subreddit.get(line_split[1])
                    if subreddit_counts is None:
                        subreddit_counts = {}
                        user_counts_by_subreddit[line_split[1]] = subreddit_counts
                        
                    user_count = subreddit_counts.get(line_split[0])
                    if user_count is None:
                        user_count = UserCount(name=line_split[0])
                        subreddit_counts[line_split[0]] = user_count
                    user_count.comments += int(line_split[2])
                    user_count.score_sum += int(line_split[3])
                    user_count.abs_score_sum += int(line_split[4])
    
    return user_counts_by_subreddit

def calculate_page_ranks():
    #for each of the top 100 subreddits:
    subreddits = ['t5_2qh1i','t5_2qh33','t5_2qqjc','t5_mouw','t5_2qh13','t5_2qh0u','t5_2qzb6','t5_2r0ij','t5_2qh03','t5_2qh1e','t5_2qh3s','t5_2qh49','t5_2qh1u','t5_2qh1o','t5_2qh3l','t5_2qt55','t5_2sokd','t5_2qm4e','t5_2sbq3','t5_2qh4i','t5_2qh6e','t5_2s5oq','t5_2ti4h','t5_2qh7d','t5_2szyo','t5_2qh87','t5_2qgzy','t5_2to41','t5_2ul7u','t5_2qh72','t5_2qh53','t5_2qgzt','t5_2qh55','t5_2qnts','t5_2tecy','t5_2t7no','t5_2qhlh','t5_2qstm','t5_2tk95','t5_2rmfx','t5_2u3ta','t5_2qxzy','t5_2qh5b','t5_2tycb','t5_2qh7a','t5_2raed','t5_2rm4d','t5_2s3nb','t5_2r2jt','t5_2qhx4','t5_2qh16','t5_2qh61','t5_2qh3v','t5_2s7tt','t5_2cneq','t5_2qh2p','t5_2qh4j','t5_2r8tu','t5_2qq5c','t5_2rfxx','t5_2r9vp','t5_2qhsa','t5_33x33','t5_2rjz2','t5_2u5kl','t5_2qhwp','t5_2qh4w','t5_2sgp1','t5_6','t5_vf2','t5_2qlqh','t5_2fwo','t5_2qh3p','t5_2va9w','t5_34jka','t5_2t5y3','t5_2r65t','t5_2s7yq','t5_2qhbe','t5_2qmeb','t5_2r5a3','t5_2rm76','t5_2x93b','t5_2w67q','t5_2qjvn','t5_2xinb','t5_2qj5n','t5_2r94o','t5_2qi58','t5_2qmg3','t5_2ssp3','t5_2qi4s','t5_2skqi','t5_2qh0s','t5_2qqlo','t5_2qo4s','t5_2qj9g','t5_2vegg','t5_2r5rp','t5_2r6rj']
    for subreddit in subreddits:
        G = pr.load_graph(AUTHOR_TO_AUTHOR_DATA_FOLDER, subreddit)
        pr_result = pr.page_rank(G)
        pr.save_results_to_file(PAGE_RANK_RESULTS_FOLDER, subreddit, pr_result)    

#receives: 
#dict {author, page_rank score}
#dict {author, UserCount}
def run_correlations(page_rank_scores, user_counts):    
    pr_list = []
    avg_score_list = []
    avg_abs_score_list = []
    comments_list = []
    for entry in page_rank_scores:
        user_count = user_counts.get(entry[0], None)
        if user_count is None:
            continue
        
        avg_score = float(user_count.score_sum) / int(user_count.comments)
        avg_abs_score = float(user_count.abs_score_sum) / int(user_count.comments)
        
        pr_list.append(float(entry[1]))        
        avg_score_list.append(avg_score)
        avg_abs_score_list.append(avg_abs_score)
        comments_list.append(int(user_count.comments))         

    correlations = []
    correlations.append(correlation("pr","avg_score", pr_list, avg_score_list))
    correlations.append(correlation("pr","avg_abs_score", pr_list, avg_abs_score_list))
    correlations.append(correlation("pr","comments", pr_list, comments_list))
    correlations.append(correlation("comments","avg_score", comments_list, avg_score_list))
    correlations.append(correlation("comments","avg_abs_score", comments_list, avg_abs_score_list))

    return correlations


def correlation(x_label, y_label, x, y):
#     x= x[0:200]
#     y= y[0:200]
#     plt.plot(x, y[0:100], 'ro')
#     plt.xlabel(x_label)
#     plt.ylabel(y_label)
#     plt.title(x_label+"-"+y_label)
#     plt.axis([0, 6, 0, 20])
#     plt.show()
    cor = pearsonr(x, y)
    return (x_label, y_label, ('{0:.4f}'.format(cor[0]), '{0:.4f}'.format(cor[1])))
    
def get_page_rank_scores(data_folder, subreddit):
    for filename in os.listdir(data_folder):
        if subreddit in filename:
            with open(data_folder+filename) as f:
                page_rank_scores = [line.split(',') for line in f]
                return page_rank_scores

def load_subreddits():
    with open(SUBREDDITS_FILE) as f:
                subreddits = [line.split(',') for line in f]
    return subreddits

def save_correlations_to_file(file_name, correlations_by_subreddit):
    #creating header row:
    header_row = "subreddit,n,"
    for cor in correlations_by_subreddit[0][1]:
        cor_name = cor[0]+"-"+cor[1]
        header_row += cor_name+"_r,"+cor_name+"_p,"
        
    #saving correlations to file
    f = open(file_name, 'w')
    write_print(f,header_row)  
    for cor_by_subreddit in correlations_by_subreddit:
        cor_str = str(cor_by_subreddit[0][0])+','+str(cor_by_subreddit[0][1])+',' #subreddit and N
        for cor in cor_by_subreddit[1]:
            cor_str += str(cor[2][0])+","+str(cor[2][1])+","
        write_print(f, cor_str)  
    
    f.close()

def write_print(f, line):
    print(line)
    f.write(line+'\n')
    
      
def main():   
    calculate_page_ranks()
    user_counts_by_subreddit = load_comment_counts(AUTHOR_COUNTS_DATA_FOLDER)
    
    #Correlation analysis
    results = []
    results_top = []
    subreddits = load_subreddits(); 
    subreddits_ids = [sub[0] for sub in subreddits]
    for subreddit in subreddits_ids:
        #Run correlations
        page_rank_scores = get_page_rank_scores(PAGE_RANK_RESULTS_FOLDER, subreddit)
        if(subreddit not in user_counts_by_subreddit): continue 
        correlations = run_correlations(page_rank_scores, user_counts_by_subreddit[subreddit])
        results.append(((subreddit, len(page_rank_scores)), correlations))
        
        #Run correlations on top 1%:
        page_rank_scores_top = page_rank_scores[0:len(page_rank_scores)/100]
        correlations_top = run_correlations(page_rank_scores_top, user_counts_by_subreddit[subreddit])
        results_top.append(((subreddit, len(page_rank_scores_top)), correlations_top)) 
        
    save_correlations_to_file(CORRELATION_RESULTS, results) ;
    save_correlations_to_file(CORRELATION_RESULTS_TOP, results_top) ;

    
if __name__ == "__main__":
    main()