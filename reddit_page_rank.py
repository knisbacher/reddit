import networkx as nx
import operator
import os
import sys
import time
import psutil


PAGE_RANK_RESULT_FILE_PREFIX = "page_rank_result_"
PAGE_RANK_RESULT_FILE_SUFFIX = ".csv"
    
 
def get_index(dict, key):
    """
    Returns the value of given key (value is an int). 
    If key doesn't exist in dict, it is set as len(dict)+1
    """    
    return dict.setdefault(key, len(dict)+1)

def load_graph(data_folder, subreddit_id):
    #Creating the graph:
    log("create graph")
    edge_count = 0
    
    G=nx.DiGraph()
    
    #Reading files line by line and creating graph:
    for filename in os.listdir(data_folder):
        if subreddit_id not in filename: 
            continue ;
        
        first_line = True
        with open(data_folder+filename) as f:
            for line in f:
                if(first_line):
                    first_line = False
                    continue 
                
                lineSplit = line.strip().split(',');
                #skip certain users:
                if(ignored_user(lineSplit[0]) or ignored_user(lineSplit[1])): continue
                
                #skip self comments:
                if(lineSplit[0] == lineSplit[1]): continue 
                
                #Adding a weighted edge from user lineSplit[0] to lineSplit[1] where 
                #weight is the number of such comments.
                G.add_edge(lineSplit[0], lineSplit[1], weight=int(lineSplit[2]))
                edge_count = edge_count+1
                if(edge_count % 1000000 == 0):
                    log("count: "+str(edge_count))
            
            log("Finished loading file: "+filename)

    log("Number of lines read:"+str(edge_count))    
    log("Finished building graph.")
    return G

def ignored_user(user_name):
    #Ignore comments with deleted users:
    if(user_name == "[deleted]"):
        return True
    #Ignore comments by bots:
    elif(user_name.lower().endswith("bot")):
        return True
    return False

def page_rank(G):
    log("# of edges: "+str(G.number_of_edges())+",# of nodes: "+str(G.number_of_nodes()))
    log("Starting page rank calculation")
    pr = nx.pagerank(G)
    log("starting sort..")
    #sort results by the pr score (dict value), then reverse for desc order:
    sorted_pr = sorted(pr.items(), key=operator.itemgetter(1))
    sorted_pr.reverse()
    log("Finished page rank.")
    return sorted_pr    

def save_results_to_file(results_folder, subreddit_id, sorted_pr): 
    results_file = results_folder+PAGE_RANK_RESULT_FILE_PREFIX+subreddit_id+PAGE_RANK_RESULT_FILE_SUFFIX
    log("Starting to write file:")
    #writing results to file:
    f = open(results_file, 'w')
    for user_score in sorted_pr:
        #writing to file and removing exponential form:
        f.write(user_score[0]+','+str('{0:.10f}'.format(user_score[1]))+'\n')  
    f.close()
    log("finished writing results to: "+results_file)

        
def log(msg):
    vm = psutil.virtual_memory()
    print("%s\t%s.\t total: %s, used: %s%%."%(time.strftime("%c"), msg,"%.3f"%(float(vm.total)/1000000000), vm.percent))


def main():   
    subreddit_id = "test" 
    data_folder = "/home/vaadrahelemenu19/reddit/pagerank/data/author_to_author_by_subreddit/" 
    G = load_graph(data_folder, subreddit_id)
    pr = page_rank(G)
    save_results_to_file(subreddit_id, pr)
    
if __name__ == "__main__":
    main()