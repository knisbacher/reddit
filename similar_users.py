import networkx as nx
import operator
import os
import sys
import time
import psutil
import re
import reddit_page_rank as pr
# import matplotlib.pyplot as plt
from scipy.stats.stats import pearsonr
from _collections import defaultdict
from operator import sub
from macpath import split
from networkx.algorithms.bipartite.basic import color


PAGE_RANK_RESULTS_FOLDER = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/page_rank_results/"
SUBREDDITS_FILE = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/subreddits.csv"
SIMILAR_FILE = "/Users/Shlomo/Dropbox/CS MSc/Introduction to Data Science/project/reddit/similar.txt"
CATEGORIES_FILE = "/Users/Shlomo/git/reddit/categories.txt"


# PAGE_RANK_BASE_FOLDER = "/home/vaadrahelemenu19/reddit/pagerank/"
# PAGE_RANK_RESULTS_FOLDER = PAGE_RANK_BASE_FOLDER +"data/page_rank_by_subreddit/"
# SUBREDDITS_FILE = "/home/vaadrahelemenu19/reddit/pagerank/src/reddit/top100.csv"



def load_file():    #loading from file:
    with open(GRAPH_FULL_FILE_PATH) as f:
        lines = f.read().splitlines()
    return lines[1:] ; #Remove headers


    """
    Returns the value of given key (value is an int). 
    If key doesn't exist in dic, it is set as len(dic)+1
    """    
def get_index(dic, key):
    return dic.setdefault(key, len(dic)+1)

        
        

def log(msg):
    print("%s\t%s."%(time.strftime("%c"), msg))

    
def load_subreddits():
    with open(SUBREDDITS_FILE) as f:
                subreddits = [line.split(',') for line in f]
    
    subreddits_dict = {}
    for sub in subreddits:
        subreddits_dict[sub[0]] = sub[1]
    
    return subreddits_dict

def load_categories():
    categories = {}
    with open(CATEGORIES_FILE) as f:
        for line in f:
            if 'sports,' in line:
                log("sports,")
            category = line.split(',')
            #Taking the first level category, 
            #e.g. 30ROCK,/subreddits/entertainment/television/30-rock/ -> [30ROCK, entertainment]
            log(category)
            categories[category[0]] = category[1].split('/')[2]
                
    
    return categories

def write_print(f, line):
    print(line)
    f.write(line+'\n')
    
def get_page_rank_top_users():
    pr_results = {}
    for filename in os.listdir(PAGE_RANK_RESULTS_FOLDER):
        if "csv" not in filename: continue ;
        first_line = True
        current_list = []
        with open(PAGE_RANK_RESULTS_FOLDER+filename) as f:
            for line in f:
                if(first_line):
                    first_line_str = line
                    first_line = False
                    continue 
                #adding all users:
                current_list.append(line.split(',')[0])
            #Taking top 1%
            current_list = current_list[0:len(current_list)/100]
            
            #adding to dict:
            split_filename = re.split('_|\.',filename)
            log(split_filename)
            id = split_filename[3]+'_'+split_filename[4]
            
            pr_results[id] = set(current_list)
    
    return pr_results
      
def main():
    log("loading subreddits and top users")
    subreddits_dict = load_subreddits()
    subreddits_ids = subreddits_dict.keys()
    subreddits = set()
    top_users_by_sub_id = get_page_rank_top_users()
    similar_subreddits = set()
    for i in subreddits_ids:
        for j in subreddits_ids:
            if(j == i):
                continue 
            
            #get the intersection of the sets:
            intersect = set.intersection(top_users_by_sub_id[i], top_users_by_sub_id[j])
            pair = None

            #normalizing connectivity by subreddit size so graph isn't too full:
            factor = 25
            max_len = max(len(top_users_by_sub_id[i]) , len(top_users_by_sub_id[j]))
            if(max_len > 5000):
                factor = 4
            elif(max_len > 3000):
                factor = 8
            elif(max_len > 1500):
                factor = 15
            
            
            if(len(intersect) > max_len / factor):
                pair = (min(subreddits_dict[i], subreddits_dict[j]),max(subreddits_dict[i], subreddits_dict[j])) 
                      
            if(pair is not None):
                if(pair[0] < pair[1]):         
                    similar_subreddits.add(pair)
                else:
                    similar_subreddits.add((pair[1], pair[0]))
                
                subreddits.add(pair[0])
                subreddits.add(pair[1])
    log("similar subreddits length:"+str(len(similar_subreddits)))
    
    #Coloring by category:
    colors = ['coral','blue','red','violet','green','purple','DarkTurquoise','orange','grey','maroon','black']
    categories_dict = load_categories()
    category_list = [categories_dict[current_sub] for current_sub in subreddits]
    category_list = list(set(category_list))
    category_to_color = {}
    for i in range(len(category_list)):
        if(i < len(colors)):
            category_to_color[category_list[i]] = colors[i]
        else:
            log("need new color for: "+str(i))
        
    log("writing to file...")
    f = open(SIMILAR_FILE, 'w')
    #writing edges:
    f.write("{color:none}"+"\n")
    for pair in similar_subreddits:
        #writing to file and removing exponential form:
        f.write(pair[0]+"--"+pair[1]+"\n")  

    
    #adding legend: 
    for i in range(len(category_list))[1:]:
        f.write(category_list[i-1]+"--"+category_list[i]+"\n")

    f.write("Category Legend: {color:black}\n")        
    for cat in category_to_color.keys():
        f.write(cat+" {color:"+category_to_color[cat]+"}\n")

    #writing node colors:
    log(category_list)
    log(str(category_to_color))
    for sub in subreddits:
        f.write(sub+" {color:"+category_to_color[categories_dict[sub]]+"}\n")
    
    f.close()
    
    log("finished")
    
# import random
# r = lambda: random.randint(0,255)
# print('#%02X%02X%02X' % (r(),r(),r()))
    
if __name__ == "__main__":
    main()